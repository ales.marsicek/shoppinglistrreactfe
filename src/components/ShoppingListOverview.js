import {Box, Button, Grid} from "@mui/material";
import ShoppingListDetail from "./ShoppingListDetail";
import {useEffect, useState} from "react";
import {DownloadShoppingList} from "../BL/ShoppingListWorker";
import {useTranslation} from "react-i18next";



export default function ShoppingListOverview(props) {
    const {i18n, t} = useTranslation();
    const [items, setItems] = useState([])
    const [showButtonText, setShowButtonText] = useState(t("ShowArchivedBtn"))
    const [showArchived, setShowArchived] = useState(false)


    const handleShowArchived = () => {
        setShowArchived(!showArchived)
       setShowButtonText(showArchived ? t("ShowArchivedBtn"):t("HideArchivedBtn"));
    }

    useEffect( () => {
        // šlo by jít přes async await pattern ale zde mi to nedává příliš smysl
         DownloadShoppingList("http://marsicek.eu/unicorn/data.json",setItems);
    }, []);

    const handleArchiveItem = name => () => {
        console.log(name);
        setItems(items => (
            items.map(item => ({
                id: item.id,
                name: item.name,
                itemList: item.itemList,
                archivated: (item.archivated || item.name === name)
            }))))
    }
    return (
        <div style={{paddingTop: 50}}>
            <Button variant="contained" onClick={handleShowArchived}>{showButtonText}</Button>
            <Grid container rowSpacing={3} columnSpacing={2} style={{paddingTop: 150}}>

                {

                    items.filter(item => {
                        if(!showArchived) {
                            return item.archivated === false
                        }
                        else
                        {
                         return true;
                        }
                    }).map((item) => (
                        <Grid item md={4}>
                            <ShoppingListDetail name={item.name} key={item.id} itemList={item.itemList} archivated={item.archivated}
                                                archiveHandler={handleArchiveItem(item.name)}/>
                        </Grid>
                    ))}
            </Grid>
        </div>
    );
}