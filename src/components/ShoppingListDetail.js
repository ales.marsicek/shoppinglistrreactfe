import React from "react";
import { Box, Button, Card, CardActions, CardContent, Checkbox, Container, Grid, Modal, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";
import { PieChart, Pie, Cell, ResponsiveContainer } from 'recharts';

export default function ShoppingListDetail(props) {
    const { t } = useTranslation();
    const [open, setOpen] = React.useState(false);
    const [items, setItems] = React.useState(props.itemList);

    // Calculate done and undone item counters
    const [doneItemCounter, setDoneItemCounter] = React.useState(
        items.filter(item => item.done).length
    );
    const [undoneItemCounter, setUndoneItemCounter] = React.useState(
        items.length - doneItemCounter
    );

    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const handleArchive = () => {
        handleClose();
        props.archiveHandler();
    }

    const handleChecked = (event) => {
        const index = items.findIndex(e => e.id.toString() === event.target.name);
        const updatedItems = [...items];
        updatedItems[index].done = !updatedItems[index].done;

        const newDoneItemCounter = updatedItems.filter(item => item.done).length;
        const newUndoneItemCounter = updatedItems.length - newDoneItemCounter;

        setDoneItemCounter(newDoneItemCounter);
        setUndoneItemCounter(newUndoneItemCounter);
        setItems(updatedItems);
    };

    // Prepare data for the Pie chart
    const graphItems = [
        { value: doneItemCounter, label: "Nakoupené položky" },
        { value: undoneItemCounter, label: "Nenakoupené položky" }
    ];
 const COLORS = ["#2196f3", "#ff3d00"];
    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: '70%',
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    };

    return (
        <div>
            <Box>
                <Container>
                    <Card sx={{ minWidth: 275 }}>
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="div">
                                {props.name}
                                <br />
                                {t("itemAmount")} {props.itemList.length}
                            </Typography>
                        </CardContent>
                        <CardActions>
                            <Button onClick={handleOpen}>{t("OpenDetailBtn")}</Button>
                        </CardActions>
                    </Card>
                </Container>
            </Box>
            <Modal open={open} onClose={handleClose}>
                <Box sx={style} style={{ overflow: "scroll", maxHeight: '80%' }}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Nákupní seznam {props.name}
                    </Typography>
                    <Grid id="modal-modal-description" sx={{ mt: 2 }}>
                        {items.map((item) => (
                            <div style={{ display: "block" }} key={item.id}>
                                <div style={{ display: 'inline-flex' }}>
                                    <Checkbox
                                        name={item.id.toString()}
                                        checked={item.done}
                                        onChange={handleChecked}
                                    />
                                    <Typography style={{ lineHeight: "5" }}>
                                        {item.amount} {item.name}
                                    </Typography>
                                </div>
                            </div>
                        ))}
                    </Grid>
                    <Button onClick={handleArchive}>Archivovat</Button>
                    <div style={{ width: '100%', height: 400 }}>
                        <ResponsiveContainer>
                            <PieChart>
                                <Pie
                                    data={graphItems}
                                    dataKey="value"
                                    cx="50%"
                                    cy="50%"
                                    labelLine={false}
                                    outerRadius={80}
                                    label={({ label }) => label}
                                >
                                {graphItems.map((entry, index) => (
                                    <Cell key={`cell-${index}`} fill={COLORS[index]} />
                                ))}
                                </Pie>
                              {/*  <Tooltip />*/}
                            </PieChart>
                        </ResponsiveContainer>
                    </div>
                </Box>
            </Modal>
        </div>
    )
}
