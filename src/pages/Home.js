import {useTranslation} from "react-i18next";

export default function Home()
{
    const {i18n, t} = useTranslation();
    return (
        <h1>{t("HomeHeader")}</h1>
    )
}