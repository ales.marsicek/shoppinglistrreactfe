import './App.css';
import ShoppingListOverview from "./components/ShoppingListOverview";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import Home from "./pages/Home";
import AddShoppingList from "./pages/AddShoppingList";
import Layout from "./Layout";
import "./i18n";


export default function App() {


    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Layout/>}>
                    <Route index element={<Home/>}/>
                    <Route path="asl" element={<AddShoppingList/>}/>
                    <Route path="sl" element={<ShoppingListOverview ></ShoppingListOverview>}/>
                </Route>
            </Routes>
        </BrowserRouter>
    );
}


