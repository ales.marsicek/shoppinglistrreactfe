const mockUpShoppingList = {
    "data":
        [
            {
                "id": "YBZIOY",
                "name": "ShoppingList #1",
                "archivated": true,
                "itemList": [
                    {
                        "id": "2LWTE3",
                        "name": "chia-seminka",
                        "amount": 77,
                        "done": false
                    },
                    {
                        "id": "O7SRHM",
                        "name": "kakao",
                        "amount": 96,
                        "done": false
                    },
                    {
                        "id": "LNE41J",
                        "name": "houby",
                        "amount": 43,
                        "done": false
                    },
                    {
                        "id": "36C0WI",
                        "name": "spagety",
                        "amount": 10,
                        "done": true
                    },
                    {
                        "id": "OPPC5H",
                        "name": "cervena-cocka",
                        "amount": 4,
                        "done": false
                    },
                    {
                        "id": "FOWHG8",
                        "name": "knedlik",
                        "amount": 65,
                        "done": false
                    },
                    {
                        "id": "3GODXJ",
                        "name": "amarant",
                        "amount": 81,
                        "done": true
                    },
                    {
                        "id": "6B0QEC",
                        "name": "tofu",
                        "amount": 68,
                        "done": false
                    },
                    {
                        "id": "W6YXMU",
                        "name": "cizrna",
                        "amount": 95,
                        "done": true
                    },
                    {
                        "id": "58Z55A",
                        "name": "mrkev",
                        "amount": 27,
                        "done": true
                    },
                    {
                        "id": "6Z4913",
                        "name": "cibule",
                        "amount": 43,
                        "done": true
                    },
                    {
                        "id": "BULV2Q",
                        "name": "oves",
                        "amount": 38,
                        "done": false
                    },
                    {
                        "id": "D0XSNM",
                        "name": "chia",
                        "amount": 77,
                        "done": true
                    },
                    {
                        "id": "PGALXE",
                        "name": "chia",
                        "amount": 6,
                        "done": false
                    }
                ]
            },
            {
                "id": "XRLGF8",
                "name": "ShoppingList #2",
                "archivated": true,
                "itemList": [
                    {
                        "id": "5RQEUQ",
                        "name": "bazalka",
                        "amount": 1,
                        "done": false
                    },
                    {
                        "id": "20BIG3",
                        "name": "susene-mleko",
                        "amount": 3,
                        "done": false
                    },
                    {
                        "id": "G70COM",
                        "name": "sojova-omacka",
                        "amount": 87,
                        "done": false
                    },
                    {
                        "id": "J6D9B1",
                        "name": "broskev",
                        "amount": 47,
                        "done": false
                    },
                    {
                        "id": "V4KXN3",
                        "name": "soba-nudle",
                        "amount": 19,
                        "done": true
                    },
                    {
                        "id": "AOGFD3",
                        "name": "repa",
                        "amount": 8,
                        "done": false
                    },
                    {
                        "id": "4R80LW",
                        "name": "marmelada",
                        "amount": 80,
                        "done": true
                    },
                    {
                        "id": "XV2N2R",
                        "name": "cerna-jerabina",
                        "amount": 12,
                        "done": true
                    },
                    {
                        "id": "RJCK1C",
                        "name": "jahly",
                        "amount": 70,
                        "done": false
                    },
                    {
                        "id": "P9VU11",
                        "name": "koriandr",
                        "amount": 30,
                        "done": false
                    }
                ]
            },
            {
                "id": "L2RXLE",
                "name": "ShoppingList #3",
                "archivated": true,
                "itemList": [
                    {
                        "id": "17BJV6",
                        "name": "cocka",
                        "amount": 33,
                        "done": false
                    },
                    {
                        "id": "6A9PEF",
                        "name": "kokosove-mleko",
                        "amount": 42,
                        "done": false
                    },
                    {
                        "id": "2Z7A0I",
                        "name": "hokkaido",
                        "amount": 70,
                        "done": true
                    },
                    {
                        "id": "AYVREJ",
                        "name": "hriby",
                        "amount": 64,
                        "done": false
                    },
                    {
                        "id": "W4YDG8",
                        "name": "sojaneza",
                        "amount": 87,
                        "done": false
                    },
                    {
                        "id": "AB0FAH",
                        "name": "hrach",
                        "amount": 80,
                        "done": true
                    },
                    {
                        "id": "X15M0Q",
                        "name": "adzuki",
                        "amount": 46,
                        "done": true
                    },
                    {
                        "id": "2CHXJE",
                        "name": "miso",
                        "amount": 35,
                        "done": false
                    },
                    {
                        "id": "F834LG",
                        "name": "kardamon",
                        "amount": 85,
                        "done": true
                    },
                    {
                        "id": "XLBG7Q",
                        "name": "hrozinky",
                        "amount": 37,
                        "done": false
                    },
                    {
                        "id": "VBF8XO",
                        "name": "chia-seminka",
                        "amount": 43,
                        "done": true
                    },
                    {
                        "id": "59O2JL",
                        "name": "celozrnna-mouka",
                        "amount": 6,
                        "done": false
                    },
                    {
                        "id": "GEIOV6",
                        "name": "cuketa",
                        "amount": 94,
                        "done": false
                    }
                ]
            },
            {
                "id": "ID8U6D",
                "name": "ShoppingList #4",
                "archivated": true,
                "itemList": [
                    {
                        "id": "1TGAES",
                        "name": "bobkovy-list",
                        "amount": 4,
                        "done": true
                    },
                    {
                        "id": "T4AV52",
                        "name": "miso",
                        "amount": 9,
                        "done": false
                    },
                    {
                        "id": "C6WMIR",
                        "name": "kokos",
                        "amount": 89,
                        "done": false
                    },
                    {
                        "id": "NFNF62",
                        "name": "paprika",
                        "amount": 62,
                        "done": false
                    },
                    {
                        "id": "JH7P47",
                        "name": "seminka",
                        "amount": 43,
                        "done": false
                    },
                    {
                        "id": "9JD59M",
                        "name": "slunecnicova-seminka",
                        "amount": 84,
                        "done": true
                    },
                    {
                        "id": "MPPLH4",
                        "name": "lopuch",
                        "amount": 90,
                        "done": true
                    },
                    {
                        "id": "S3ASIK",
                        "name": "datle",
                        "amount": 45,
                        "done": true
                    },
                    {
                        "id": "X7KA0A",
                        "name": "ryzove-nudle",
                        "amount": 16,
                        "done": true
                    },
                    {
                        "id": "HZ27QM",
                        "name": "chia-seminka",
                        "amount": 87,
                        "done": true
                    },
                    {
                        "id": "G9I72F",
                        "name": "vodnice",
                        "amount": 33,
                        "done": false
                    },
                    {
                        "id": "KPJ3NA",
                        "name": "amazake",
                        "amount": 53,
                        "done": true
                    },
                    {
                        "id": "AKZ86Q",
                        "name": "chia",
                        "amount": 74,
                        "done": false
                    },
                    {
                        "id": "WX8IJR",
                        "name": "citronova-trava",
                        "amount": 66,
                        "done": false
                    },
                    {
                        "id": "518SKC",
                        "name": "kokosove-mleko",
                        "amount": 67,
                        "done": true
                    },
                    {
                        "id": "WX2X9F",
                        "name": "kurkuma",
                        "amount": 83,
                        "done": true
                    },
                    {
                        "id": "POOA6H",
                        "name": "pohanka",
                        "amount": 79,
                        "done": true
                    },
                    {
                        "id": "TFQVAL",
                        "name": "kastany",
                        "amount": 11,
                        "done": true
                    }
                ]
            },
            {
                "id": "1CXARC",
                "name": "ShoppingList #5",
                "archivated": false,
                "itemList": [
                    {
                        "id": "19WFMG",
                        "name": "cirok",
                        "amount": 69,
                        "done": true
                    },
                    {
                        "id": "HICZST",
                        "name": "sezam",
                        "amount": 26,
                        "done": false
                    },
                    {
                        "id": "66O53S",
                        "name": "ovesne-vlocky",
                        "amount": 63,
                        "done": false
                    },
                    {
                        "id": "Y63PWV",
                        "name": "umeboska",
                        "amount": 11,
                        "done": true
                    },
                    {
                        "id": "TNOLJP",
                        "name": "vlasske-orechy",
                        "amount": 5,
                        "done": false
                    },
                    {
                        "id": "U61UUV",
                        "name": "falafel",
                        "amount": 42,
                        "done": false
                    },
                    {
                        "id": "7J57ZY",
                        "name": "jarni-cibulka",
                        "amount": 81,
                        "done": false
                    }
                ]
            },
            {
                "id": "LCFS09",
                "name": "ShoppingList #6",
                "archivated": true,
                "itemList": [
                    {
                        "id": "NN09TS",
                        "name": "redkvicka",
                        "amount": 54,
                        "done": false
                    },
                    {
                        "id": "TQR4QY",
                        "name": "bila-redkev",
                        "amount": 62,
                        "done": false
                    },
                    {
                        "id": "VDTEIB",
                        "name": "broskve",
                        "amount": 51,
                        "done": false
                    },
                    {
                        "id": "900LG3",
                        "name": "mak",
                        "amount": 72,
                        "done": true
                    },
                    {
                        "id": "2FL95M",
                        "name": "polenta",
                        "amount": 24,
                        "done": true
                    },
                    {
                        "id": "XUFS0Y",
                        "name": "shoyu",
                        "amount": 78,
                        "done": true
                    },
                    {
                        "id": "AC640K",
                        "name": "polenta",
                        "amount": 13,
                        "done": true
                    },
                    {
                        "id": "CBFXYU",
                        "name": "knedlik",
                        "amount": 15,
                        "done": false
                    },
                    {
                        "id": "LFEG17",
                        "name": "wakame",
                        "amount": 38,
                        "done": false
                    },
                    {
                        "id": "LRJG74",
                        "name": "kroupy",
                        "amount": 31,
                        "done": false
                    },
                    {
                        "id": "BGFXOA",
                        "name": "celozrnna-mouka",
                        "amount": 6,
                        "done": true
                    }
                ]
            },
            {
                "id": "3QAFTV",
                "name": "ShoppingList #7",
                "archivated": false,
                "itemList": [
                    {
                        "id": "D6OJEY",
                        "name": "lnene-seminko",
                        "amount": 35,
                        "done": false
                    },
                    {
                        "id": "49NEBH",
                        "name": "chia-seminka",
                        "amount": 12,
                        "done": true
                    },
                    {
                        "id": "3GL9KG",
                        "name": "hatomugi",
                        "amount": 74,
                        "done": false
                    },
                    {
                        "id": "15A6OB",
                        "name": "sojova-smetana",
                        "amount": 13,
                        "done": false
                    },
                    {
                        "id": "ZZMEV4",
                        "name": "cibule",
                        "amount": 21,
                        "done": true
                    },
                    {
                        "id": "5TYR10",
                        "name": "spaldova-mouka",
                        "amount": 61,
                        "done": false
                    },
                    {
                        "id": "80DFR8",
                        "name": "turin",
                        "amount": 73,
                        "done": true
                    },
                    {
                        "id": "LI4PT3",
                        "name": "hliva-ustricna",
                        "amount": 21,
                        "done": false
                    },
                    {
                        "id": "RFQAQB",
                        "name": "citronova-kura",
                        "amount": 32,
                        "done": false
                    },
                    {
                        "id": "YQAP1D",
                        "name": "kedluben",
                        "amount": 59,
                        "done": false
                    },
                    {
                        "id": "3JDR15",
                        "name": "karob",
                        "amount": 44,
                        "done": false
                    },
                    {
                        "id": "AYBFNB",
                        "name": "citronova-stava",
                        "amount": 17,
                        "done": false
                    },
                    {
                        "id": "X22EIW",
                        "name": "celer",
                        "amount": 57,
                        "done": true
                    }
                ]
            },
            {
                "id": "XJU25C",
                "name": "ShoppingList #8",
                "archivated": true,
                "itemList": [
                    {
                        "id": "MJIUHF",
                        "name": "hraska",
                        "amount": 8,
                        "done": true
                    },
                    {
                        "id": "MERL1G",
                        "name": "cerny-sezam",
                        "amount": 77,
                        "done": false
                    },
                    {
                        "id": "TWRKZ4",
                        "name": "hatomugi",
                        "amount": 11,
                        "done": true
                    },
                    {
                        "id": "J4RRGH",
                        "name": "oves",
                        "amount": 78,
                        "done": false
                    },
                    {
                        "id": "JKD3FJ",
                        "name": "hrozinky",
                        "amount": 86,
                        "done": true
                    },
                    {
                        "id": "J53T99",
                        "name": "kren",
                        "amount": 34,
                        "done": false
                    },
                    {
                        "id": "QRUFUY",
                        "name": "kapusticky",
                        "amount": 12,
                        "done": true
                    }
                ]
            },
            {
                "id": "SAUW4F",
                "name": "ShoppingList #9",
                "archivated": false,
                "itemList": [
                    {
                        "id": "9YJYQD",
                        "name": "kren",
                        "amount": 74,
                        "done": true
                    },
                    {
                        "id": "BCD19U",
                        "name": "chia",
                        "amount": 51,
                        "done": false
                    },
                    {
                        "id": "2Y8A6L",
                        "name": "redkvicka",
                        "amount": 16,
                        "done": false
                    },
                    {
                        "id": "QV4MCL",
                        "name": "mrkev",
                        "amount": 14,
                        "done": true
                    },
                    {
                        "id": "CUD047",
                        "name": "cesnacek",
                        "amount": 97,
                        "done": false
                    },
                    {
                        "id": "KB6W3N",
                        "name": "majoranka",
                        "amount": 1,
                        "done": false
                    },
                    {
                        "id": "1ZI5E3",
                        "name": "jecmen",
                        "amount": 39,
                        "done": false
                    },
                    {
                        "id": "D520CD",
                        "name": "miso-pasta",
                        "amount": 71,
                        "done": true
                    },
                    {
                        "id": "NQ4DLB",
                        "name": "kokos",
                        "amount": 48,
                        "done": true
                    },
                    {
                        "id": "753YTJ",
                        "name": "fenykl",
                        "amount": 83,
                        "done": true
                    },
                    {
                        "id": "R2VOY7",
                        "name": "arasidove-maslo",
                        "amount": 22,
                        "done": false
                    },
                    {
                        "id": "M19URN",
                        "name": "topinambury",
                        "amount": 60,
                        "done": true
                    },
                    {
                        "id": "S4OD5Q",
                        "name": "mandle",
                        "amount": 85,
                        "done": true
                    },
                    {
                        "id": "7XDG8Z",
                        "name": "redkev",
                        "amount": 48,
                        "done": false
                    },
                    {
                        "id": "5YT073",
                        "name": "quinoa",
                        "amount": 16,
                        "done": false
                    },
                    {
                        "id": "23HM4J",
                        "name": "cirok",
                        "amount": 77,
                        "done": false
                    },
                    {
                        "id": "RCW8WH",
                        "name": "broskev",
                        "amount": 73,
                        "done": true
                    }
                ]
            },
            {
                "id": "O1F74D",
                "name": "ShoppingList #10",
                "archivated": false,
                "itemList": [
                    {
                        "id": "FRCD9D",
                        "name": "kardamon",
                        "amount": 77,
                        "done": false
                    },
                    {
                        "id": "AXFPMV",
                        "name": "kvasene-zeli",
                        "amount": 43,
                        "done": false
                    },
                    {
                        "id": "XEFWVW",
                        "name": "lopuch",
                        "amount": 48,
                        "done": false
                    },
                    {
                        "id": "WLNKG8",
                        "name": "jabka",
                        "amount": 36,
                        "done": true
                    },
                    {
                        "id": "XUOCB5",
                        "name": "bobkovy-list",
                        "amount": 27,
                        "done": true
                    },
                    {
                        "id": "X7NK96",
                        "name": "kedlubna",
                        "amount": 81,
                        "done": true
                    },
                    {
                        "id": "QZKN63",
                        "name": "bile-zeli",
                        "amount": 98,
                        "done": false
                    },
                    {
                        "id": "FPQNM8",
                        "name": "soba-nudle",
                        "amount": 12,
                        "done": false
                    },
                    {
                        "id": "EULAOR",
                        "name": "hriby",
                        "amount": 70,
                        "done": true
                    },
                    {
                        "id": "ZFZUCA",
                        "name": "amazake",
                        "amount": 54,
                        "done": true
                    }
                ]
            },
            {
                "id": "QN512Q",
                "name": "ShoppingList #11",
                "archivated": true,
                "itemList": [
                    {
                        "id": "MIK9HK",
                        "name": "chia",
                        "amount": 75,
                        "done": true
                    },
                    {
                        "id": "463MFM",
                        "name": "hliva",
                        "amount": 6,
                        "done": false
                    },
                    {
                        "id": "APH9A3",
                        "name": "horcicne-seminko",
                        "amount": 54,
                        "done": false
                    },
                    {
                        "id": "0QSSTM",
                        "name": "redkvicky",
                        "amount": 70,
                        "done": true
                    },
                    {
                        "id": "4UB2VZ",
                        "name": "celozrnna-ryze",
                        "amount": 94,
                        "done": false
                    },
                    {
                        "id": "Y9VGCF",
                        "name": "bila-redkev",
                        "amount": 62,
                        "done": false
                    },
                    {
                        "id": "AAKN6B",
                        "name": "mungo",
                        "amount": 43,
                        "done": false
                    },
                    {
                        "id": "C6MAZ3",
                        "name": "hummus",
                        "amount": 9,
                        "done": true
                    },
                    {
                        "id": "Q6CPON",
                        "name": "testoviny",
                        "amount": 30,
                        "done": true
                    },
                    {
                        "id": "27ZSR8",
                        "name": "vlocky",
                        "amount": 76,
                        "done": false
                    },
                    {
                        "id": "D4FU57",
                        "name": "kurkuma",
                        "amount": 56,
                        "done": true
                    },
                    {
                        "id": "QLD7R0",
                        "name": "soba-nudle",
                        "amount": 5,
                        "done": true
                    },
                    {
                        "id": "ROGVAL",
                        "name": "agar",
                        "amount": 74,
                        "done": false
                    },
                    {
                        "id": "9DEOP4",
                        "name": "hruska",
                        "amount": 6,
                        "done": true
                    },
                    {
                        "id": "M24742",
                        "name": "lnene-seminko",
                        "amount": 36,
                        "done": true
                    },
                    {
                        "id": "GG98P5",
                        "name": "mungo",
                        "amount": 25,
                        "done": true
                    },
                    {
                        "id": "V0HKZ0",
                        "name": "ovesne-vlocky",
                        "amount": 90,
                        "done": false
                    },
                    {
                        "id": "I0BQ57",
                        "name": "susene-mleko",
                        "amount": 62,
                        "done": true
                    },
                    {
                        "id": "XI9IVA",
                        "name": "citronova-stava",
                        "amount": 60,
                        "done": true
                    },
                    {
                        "id": "NP2STV",
                        "name": "cesnek",
                        "amount": 96,
                        "done": true
                    },
                    {
                        "id": "CTZ99E",
                        "name": "misopasta",
                        "amount": 37,
                        "done": false
                    }
                ]
            },
            {
                "id": "5HWGDC",
                "name": "ShoppingList #12",
                "archivated": false,
                "itemList": [
                    {
                        "id": "6BFEVA",
                        "name": "kedlubnove-zeli",
                        "amount": 23,
                        "done": false
                    },
                    {
                        "id": "OY2HVQ",
                        "name": "citronova-stava",
                        "amount": 14,
                        "done": false
                    },
                    {
                        "id": "D2FTX5",
                        "name": "chilli",
                        "amount": 82,
                        "done": true
                    },
                    {
                        "id": "6TOKED",
                        "name": "fazolky-mungo",
                        "amount": 80,
                        "done": false
                    },
                    {
                        "id": "KX9OAY",
                        "name": "amasake",
                        "amount": 62,
                        "done": true
                    },
                    {
                        "id": "UVYK8U",
                        "name": "redkvicka",
                        "amount": 23,
                        "done": false
                    },
                    {
                        "id": "1E2PR8",
                        "name": "ovesne-vlocky",
                        "amount": 26,
                        "done": true
                    },
                    {
                        "id": "PN9XU6",
                        "name": "jabka",
                        "amount": 80,
                        "done": true
                    },
                    {
                        "id": "V79XM5",
                        "name": "cerna-jerabina",
                        "amount": 6,
                        "done": false
                    },
                    {
                        "id": "30QE74",
                        "name": "ryzove-nudle",
                        "amount": 71,
                        "done": false
                    },
                    {
                        "id": "E3LU1X",
                        "name": "cesnek",
                        "amount": 31,
                        "done": true
                    },
                    {
                        "id": "80DCBZ",
                        "name": "brokolice",
                        "amount": 94,
                        "done": true
                    },
                    {
                        "id": "N9M72D",
                        "name": "aronie",
                        "amount": 69,
                        "done": true
                    },
                    {
                        "id": "GW0045",
                        "name": "bila-redkev",
                        "amount": 55,
                        "done": true
                    },
                    {
                        "id": "JPP2LR",
                        "name": "bezpluchy-oves",
                        "amount": 19,
                        "done": false
                    },
                    {
                        "id": "F02MW8",
                        "name": "redkvicka",
                        "amount": 55,
                        "done": false
                    },
                    {
                        "id": "ADVHRT",
                        "name": "kuzu",
                        "amount": 14,
                        "done": false
                    },
                    {
                        "id": "U5M1CL",
                        "name": "jablko",
                        "amount": 40,
                        "done": true
                    }
                ]
            },
            {
                "id": "5BPXVX",
                "name": "ShoppingList #13",
                "archivated": false,
                "itemList": [
                    {
                        "id": "VLAXWS",
                        "name": "jablka",
                        "amount": 74,
                        "done": true
                    },
                    {
                        "id": "CIG259",
                        "name": "cinske-zeli",
                        "amount": 28,
                        "done": false
                    },
                    {
                        "id": "WPX6UK",
                        "name": "bila-redkev",
                        "amount": 6,
                        "done": false
                    },
                    {
                        "id": "TYSLKF",
                        "name": "natto",
                        "amount": 37,
                        "done": false
                    },
                    {
                        "id": "7WJGXL",
                        "name": "rozmaryn",
                        "amount": 19,
                        "done": true
                    },
                    {
                        "id": "VW59BC",
                        "name": "knedlik",
                        "amount": 35,
                        "done": true
                    },
                    {
                        "id": "TU8XG9",
                        "name": "seitan",
                        "amount": 29,
                        "done": true
                    },
                    {
                        "id": "CGON8I",
                        "name": "redkvicky",
                        "amount": 34,
                        "done": true
                    },
                    {
                        "id": "VQ6SNZ",
                        "name": "horcice",
                        "amount": 16,
                        "done": false
                    },
                    {
                        "id": "YFIDPI",
                        "name": "kvetak",
                        "amount": 27,
                        "done": false
                    },
                    {
                        "id": "S3G4RO",
                        "name": "vodnice",
                        "amount": 78,
                        "done": false
                    },
                    {
                        "id": "XNG07Q",
                        "name": "chia-seminka",
                        "amount": 6,
                        "done": true
                    },
                    {
                        "id": "6QFTSR",
                        "name": "broskve",
                        "amount": 7,
                        "done": false
                    },
                    {
                        "id": "G26S6I",
                        "name": "mungo-fazolky",
                        "amount": 73,
                        "done": true
                    },
                    {
                        "id": "S70U42",
                        "name": "slunecnice",
                        "amount": 97,
                        "done": true
                    },
                    {
                        "id": "6JNPD7",
                        "name": "kuskus",
                        "amount": 24,
                        "done": false
                    }
                ]
            },
            {
                "id": "715IKK",
                "name": "ShoppingList #14",
                "archivated": false,
                "itemList": [
                    {
                        "id": "XPBLKC",
                        "name": "zelene-fazolky",
                        "amount": 16,
                        "done": true
                    },
                    {
                        "id": "UX4Q2J",
                        "name": "adzuki",
                        "amount": 95,
                        "done": false
                    },
                    {
                        "id": "09PNBW",
                        "name": "amarant",
                        "amount": 95,
                        "done": true
                    },
                    {
                        "id": "50ENY4",
                        "name": "cesnacek",
                        "amount": 42,
                        "done": true
                    },
                    {
                        "id": "LHJDM5",
                        "name": "ryzovy-ocet",
                        "amount": 45,
                        "done": false
                    },
                    {
                        "id": "FUIEPT",
                        "name": "kapusticky",
                        "amount": 9,
                        "done": true
                    },
                    {
                        "id": "D57X0K",
                        "name": "turin",
                        "amount": 55,
                        "done": false
                    },
                    {
                        "id": "R0PHT0",
                        "name": "cervena-cocka",
                        "amount": 13,
                        "done": false
                    },
                    {
                        "id": "MPJ7VH",
                        "name": "broskve",
                        "amount": 73,
                        "done": false
                    },
                    {
                        "id": "R70M2M",
                        "name": "cinske-zeli",
                        "amount": 62,
                        "done": true
                    }
                ]
            },
            {
                "id": "C9WSKI",
                "name": "ShoppingList #15",
                "archivated": false,
                "itemList": [
                    {
                        "id": "CTH8DP",
                        "name": "seminka",
                        "amount": 100,
                        "done": false
                    },
                    {
                        "id": "05JBV9",
                        "name": "fazole",
                        "amount": 17,
                        "done": false
                    },
                    {
                        "id": "GU64CV",
                        "name": "mungo",
                        "amount": 84,
                        "done": true
                    },
                    {
                        "id": "UUKDQN",
                        "name": "amarant",
                        "amount": 16,
                        "done": true
                    },
                    {
                        "id": "60U48X",
                        "name": "oriskove-maslo",
                        "amount": 34,
                        "done": true
                    },
                    {
                        "id": "NMBXQW",
                        "name": "hrach",
                        "amount": 92,
                        "done": true
                    },
                    {
                        "id": "DB92PB",
                        "name": "bila-redkev",
                        "amount": 30,
                        "done": true
                    },
                    {
                        "id": "742MU0",
                        "name": "kren",
                        "amount": 39,
                        "done": false
                    },
                    {
                        "id": "KJWO8E",
                        "name": "polenta",
                        "amount": 68,
                        "done": false
                    },
                    {
                        "id": "RF4U5J",
                        "name": "aronie",
                        "amount": 10,
                        "done": true
                    },
                    {
                        "id": "G2JPDW",
                        "name": "vlasske-orechy",
                        "amount": 70,
                        "done": true
                    },
                    {
                        "id": "QMBZ5J",
                        "name": "ovocenka",
                        "amount": 65,
                        "done": false
                    },
                    {
                        "id": "TMB5EF",
                        "name": "kokosovy-tuk",
                        "amount": 26,
                        "done": true
                    },
                    {
                        "id": "4I6Q85",
                        "name": "natto-miso",
                        "amount": 95,
                        "done": true
                    },
                    {
                        "id": "J4AIE8",
                        "name": "kaderavek",
                        "amount": 93,
                        "done": true
                    },
                    {
                        "id": "UGQA1Y",
                        "name": "dynova-seminka",
                        "amount": 17,
                        "done": false
                    },
                    {
                        "id": "OUUOOS",
                        "name": "horcicne-seminko",
                        "amount": 41,
                        "done": true
                    }
                ]
            },
            {
                "id": "WBQRIO",
                "name": "ShoppingList #16",
                "archivated": false,
                "itemList": [
                    {
                        "id": "LT4YUI",
                        "name": "tymian",
                        "amount": 52,
                        "done": false
                    },
                    {
                        "id": "278N80",
                        "name": "soba-nudle",
                        "amount": 8,
                        "done": false
                    },
                    {
                        "id": "5I1W77",
                        "name": "burizony",
                        "amount": 74,
                        "done": false
                    },
                    {
                        "id": "SUV5V8",
                        "name": "kokos",
                        "amount": 49,
                        "done": true
                    },
                    {
                        "id": "M825BE",
                        "name": "redkvicka",
                        "amount": 81,
                        "done": false
                    },
                    {
                        "id": "DMTA7I",
                        "name": "kapusta",
                        "amount": 16,
                        "done": false
                    },
                    {
                        "id": "KOA3N6",
                        "name": "cervena-paprika",
                        "amount": 3,
                        "done": false
                    },
                    {
                        "id": "JOHA37",
                        "name": "burizony",
                        "amount": 11,
                        "done": false
                    },
                    {
                        "id": "4XEUKP",
                        "name": "turin",
                        "amount": 97,
                        "done": false
                    },
                    {
                        "id": "MXFBP9",
                        "name": "bulgur",
                        "amount": 88,
                        "done": true
                    },
                    {
                        "id": "T58FVX",
                        "name": "tofuneza",
                        "amount": 67,
                        "done": false
                    },
                    {
                        "id": "9Y2SVT",
                        "name": "susene-houby",
                        "amount": 39,
                        "done": false
                    },
                    {
                        "id": "062C4R",
                        "name": "cizrna",
                        "amount": 76,
                        "done": false
                    },
                    {
                        "id": "4N9TKJ",
                        "name": "citronova-stava",
                        "amount": 43,
                        "done": true
                    },
                    {
                        "id": "ACCU4B",
                        "name": "agar",
                        "amount": 75,
                        "done": true
                    }
                ]
            },
            {
                "id": "6RM41A",
                "name": "ShoppingList #17",
                "archivated": true,
                "itemList": [
                    {
                        "id": "WR0VXF",
                        "name": "wakame",
                        "amount": 78,
                        "done": false
                    },
                    {
                        "id": "ONKXQ4",
                        "name": "kokosove-mleko",
                        "amount": 47,
                        "done": true
                    },
                    {
                        "id": "NXOHD1",
                        "name": "brokolice",
                        "amount": 44,
                        "done": false
                    },
                    {
                        "id": "TRH12H",
                        "name": "hruska",
                        "amount": 32,
                        "done": true
                    },
                    {
                        "id": "5JURKM",
                        "name": "horcicne-seminko",
                        "amount": 30,
                        "done": true
                    },
                    {
                        "id": "AWV4XX",
                        "name": "zeli",
                        "amount": 36,
                        "done": false
                    },
                    {
                        "id": "VK1F56",
                        "name": "cibule",
                        "amount": 88,
                        "done": false
                    },
                    {
                        "id": "40QA7Q",
                        "name": "miso-pasta",
                        "amount": 33,
                        "done": false
                    },
                    {
                        "id": "EHJBUB",
                        "name": "cuketa",
                        "amount": 82,
                        "done": true
                    },
                    {
                        "id": "YNPVL1",
                        "name": "broskve",
                        "amount": 47,
                        "done": false
                    },
                    {
                        "id": "T0WDYM",
                        "name": "svestky",
                        "amount": 84,
                        "done": true
                    },
                    {
                        "id": "OZHKY1",
                        "name": "burske-orechy",
                        "amount": 42,
                        "done": false
                    },
                    {
                        "id": "LZ7X34",
                        "name": "cerne-fazole",
                        "amount": 93,
                        "done": true
                    },
                    {
                        "id": "O27L24",
                        "name": "kinpira",
                        "amount": 57,
                        "done": false
                    },
                    {
                        "id": "ENRHO4",
                        "name": "bila-redkev",
                        "amount": 96,
                        "done": true
                    },
                    {
                        "id": "YMFC5O",
                        "name": "azuki",
                        "amount": 60,
                        "done": false
                    },
                    {
                        "id": "EOPD70",
                        "name": "natto-miso",
                        "amount": 62,
                        "done": true
                    },
                    {
                        "id": "BF7SAE",
                        "name": "ovocenka",
                        "amount": 56,
                        "done": true
                    }
                ]
            },
            {
                "id": "BMQWG6",
                "name": "ShoppingList #18",
                "archivated": false,
                "itemList": [
                    {
                        "id": "GU8ZXU",
                        "name": "redkev",
                        "amount": 76,
                        "done": false
                    },
                    {
                        "id": "VVH3YK",
                        "name": "cervena-repa",
                        "amount": 94,
                        "done": false
                    },
                    {
                        "id": "Q00DOC",
                        "name": "cevena-cibule",
                        "amount": 93,
                        "done": true
                    },
                    {
                        "id": "R9HQMS",
                        "name": "vlocky",
                        "amount": 56,
                        "done": false
                    },
                    {
                        "id": "W0PDB5",
                        "name": "natto-miso",
                        "amount": 77,
                        "done": true
                    },
                    {
                        "id": "HH1D4T",
                        "name": "chia-seminka",
                        "amount": 8,
                        "done": true
                    },
                    {
                        "id": "3ZXG28",
                        "name": "cirok",
                        "amount": 23,
                        "done": true
                    },
                    {
                        "id": "H398FY",
                        "name": "natto",
                        "amount": 47,
                        "done": false
                    }
                ]
            },
            {
                "id": "BSJU6D",
                "name": "ShoppingList #19",
                "archivated": true,
                "itemList": [
                    {
                        "id": "UG40T5",
                        "name": "kokos",
                        "amount": 80,
                        "done": true
                    },
                    {
                        "id": "GOAIJ1",
                        "name": "bile-zeli",
                        "amount": 93,
                        "done": true
                    },
                    {
                        "id": "8CCDWX",
                        "name": "amasake",
                        "amount": 88,
                        "done": true
                    },
                    {
                        "id": "3ZW5T6",
                        "name": "jahody",
                        "amount": 74,
                        "done": false
                    },
                    {
                        "id": "68J40Z",
                        "name": "kardamon",
                        "amount": 21,
                        "done": false
                    },
                    {
                        "id": "C1XK4H",
                        "name": "seminka",
                        "amount": 60,
                        "done": true
                    },
                    {
                        "id": "5QWSAX",
                        "name": "chia-seminka",
                        "amount": 42,
                        "done": true
                    },
                    {
                        "id": "8U02BA",
                        "name": "redkvicka",
                        "amount": 76,
                        "done": false
                    },
                    {
                        "id": "NS2JL7",
                        "name": "tahini",
                        "amount": 89,
                        "done": false
                    },
                    {
                        "id": "AZVTW7",
                        "name": "broskve",
                        "amount": 22,
                        "done": false
                    },
                    {
                        "id": "15IB5S",
                        "name": "badyan",
                        "amount": 82,
                        "done": true
                    },
                    {
                        "id": "D1XHKC",
                        "name": "umeboska",
                        "amount": 78,
                        "done": true
                    },
                    {
                        "id": "FU6J9N",
                        "name": "cervena-cocka",
                        "amount": 65,
                        "done": false
                    },
                    {
                        "id": "368LDY",
                        "name": "bulgur",
                        "amount": 13,
                        "done": false
                    },
                    {
                        "id": "QOS69U",
                        "name": "tofuneza",
                        "amount": 85,
                        "done": true
                    }
                ]
            },
            {
                "id": "PV91WE",
                "name": "ShoppingList #20",
                "archivated": true,
                "itemList": [
                    {
                        "id": "KC9WUJ",
                        "name": "cerna-fazole",
                        "amount": 46,
                        "done": true
                    },
                    {
                        "id": "YMIUOB",
                        "name": "cerny-sezam",
                        "amount": 75,
                        "done": false
                    },
                    {
                        "id": "3Q99HN",
                        "name": "cerne-fazole",
                        "amount": 81,
                        "done": true
                    },
                    {
                        "id": "DH99MN",
                        "name": "slunecnicova-seminka",
                        "amount": 51,
                        "done": true
                    },
                    {
                        "id": "GG60BO",
                        "name": "kopr",
                        "amount": 85,
                        "done": false
                    },
                    {
                        "id": "XQ4LFB",
                        "name": "cinske-zeli",
                        "amount": 84,
                        "done": false
                    },
                    {
                        "id": "S1YDUZ",
                        "name": "tempeh",
                        "amount": 48,
                        "done": true
                    },
                    {
                        "id": "KBKF83",
                        "name": "pohankova-mouka",
                        "amount": 43,
                        "done": true
                    },
                    {
                        "id": "CSMEMK",
                        "name": "broskve",
                        "amount": 47,
                        "done": false
                    },
                    {
                        "id": "V0SIH7",
                        "name": "medvedi-cesnek",
                        "amount": 49,
                        "done": true
                    },
                    {
                        "id": "TZOBFM",
                        "name": "seitan",
                        "amount": 35,
                        "done": false
                    },
                    {
                        "id": "HH1HTJ",
                        "name": "fazolky",
                        "amount": 7,
                        "done": true
                    },
                    {
                        "id": "YTR0VA",
                        "name": "jablka",
                        "amount": 96,
                        "done": true
                    },
                    {
                        "id": "FCW6JS",
                        "name": "tofuneza",
                        "amount": 41,
                        "done": true
                    },
                    {
                        "id": "K4XFS5",
                        "name": "brokolice",
                        "amount": 56,
                        "done": true
                    },
                    {
                        "id": "17DNIO",
                        "name": "paprika",
                        "amount": 47,
                        "done": true
                    },
                    {
                        "id": "X63MH0",
                        "name": "citron",
                        "amount": 50,
                        "done": true
                    },
                    {
                        "id": "1XCZR6",
                        "name": "shoyu",
                        "amount": 85,
                        "done": true
                    },
                    {
                        "id": "IVKT16",
                        "name": "redkvicka",
                        "amount": 22,
                        "done": false
                    }
                ]
            }
        ]
}

function DownloadShoppingList(url, callback) {
    fetch(url,{signal:AbortSignal.timeout(300)}).then((resp) => {
        if (resp.ok) {
            return (resp.json()).data;
        } else {
            // zapíšu chybu přes logger abych věděl, co se stalo
            callback(mockUpShoppingList.data);
        }
    }).then((data) => {
        callback(data);
    }).catch((err) => {
        // zapíšu chybu do logu
        callback(mockUpShoppingList.data);
    })
}

function UpdateData(url, jsonObj, callback) {
    const requestOptions = {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: jsonObj
    };

    fetch(url, requestOptions).then((resp) => {
        return resp.ok ? resp.json() : "";
    }).then(data => {
        // zde provedu již co je potřeba s datay
    }).catch(err => {
        // opět zaloguji chybu
    });


}

export {DownloadShoppingList}
