import { Outlet, Link } from "react-router-dom";
import {Box, Button, createTheme, CssBaseline, Grid, MenuItem, ThemeProvider} from "@mui/material";
import { useState } from "react";
import { useTranslation } from "react-i18next";

export default function Layout() {
    const lightTheme = createTheme({ palette: { mode: "light" } });
    const darkTheme = createTheme({ palette: { mode: "dark" } });
    const [theme, changeTheme] = useState(lightTheme);
    const { i18n, t } = useTranslation();
    const LANGUAGES = [
        { label: "Čeština", code: "cs" },
        { label: "English", code: "en" }
    ];

    const changeThemeClick = () => {
        const newTheme = theme.palette.mode === "light" ? darkTheme : lightTheme;
        changeTheme(newTheme);
    };

    const onChangeLang = (e) => {
        const lang_code = e.target.value;
        i18n.changeLanguage(lang_code);
    };

    return (
        <ThemeProvider theme={theme}>

            <CssBaseline />
            <Box style={{paddingLeft:50,paddingRight:50,paddingTop:20}}>
                <Box style={{marginLeft:'87%'}}>
            <Button onClick={changeThemeClick}>{t("ChangeColorBtn")}</Button>
            <select defaultValue={i18n.language} onChange={onChangeLang} style={{ marginLeft: '10px' }}>
                {LANGUAGES.map(({ code, label }) => (
                    <option key={code} value={code}>
                        {label}
                    </option>
                ))}
            </select>
                </Box>
            <Box sx={{ display: 'flex', gap: 2, marginTop: 2, alignItems: 'center', color:"#FFF", backgroundColor:"#1769aa" }}>
                <MenuItem component={Link} to="/">
                    {t("Home")}
                </MenuItem>
                <MenuItem component={Link} to="/asl">
                    {t("AddNewSL")}
                </MenuItem>
                <MenuItem component={Link} to="/sl">
                    {t("SLOverview")}
                </MenuItem>
            </Box>
            <Outlet />
            </Box>
        </ThemeProvider>
    );
}
