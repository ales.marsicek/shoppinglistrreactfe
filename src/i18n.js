import i18n from "i18next";
import { initReactI18next } from "react-i18next";

i18n.use(initReactI18next).init({
    lng: "cs",
    fallbackLng: "en",
    interpolation: {
        escapeValue: false,
    },
    resources: {
        en: {
            translation: {
                title: 'Multi-language app',
                Home:"Home",
                HomeHeader: "Welcome in shopping list application",
                AddNewSL:"Add new shopping list",
                SLOverview: "Shopping list overview",
                NotImplemented:"Not implemented",
                OpenDetailBtn: "Open detail",
                ShowArchivedBtn: "Show archived",
                HideArchivedBtn:"Hide archived",
                ChangeColorBtn:"Change color scheme",
                itemAmount:"Shopping list amount: "
            }
        },
        cs:{
            translation: {
                Home:"Domů",
                HomeHeader:"Vítejte v aplikaci nákupních seznamů",
                AddNewSL: "Přidat nákupní seznam",
                SLOverview: "Přehled nákupních seznamů",
                NotImplemented:"Není implementováno",
                OpenDetailBtn: "otevřít detail",
                ShowArchivedBtn: "ukázat archivované",
                HideArchivedBtn:"schovat archivované",
                ChangeColorBtn:"změnit barevné schéma",
                itemAmount:"Počet položek: "
            }
        }
    },
});

export default i18n;